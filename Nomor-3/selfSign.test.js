/// <reference types="cypress" />

describe('self sign test', () => {
    it('Self sign', () => {
        cy.viewport(1280, 720);
        cy.visit('app.privy.id');
        cy.wait(3000);
        cy.get('[id=__BVID__4]').type('ini id');
        cy.contains('CONTINUE').click();
        cy.get('[id=__BVID__6]').type('ini pw');
        cy.contains('CONTINUE').click();
        cy.get('[id=v-step-0]').click();
        cy.contains('Self Sign').click();
        cy.wait(2000);
        cy.get('input[type="file"]').attachFile('test.pdf',{ subjectType: 'drag-n-drop' });
        cy.get('footer#__BVID__116___BV_modal_footer_.modal-footer > button.btn.btn-danger').click();
        cy.get('button[id="step-document-1"]',{timeout:20000}).click();
        cy.contains('Done').click();
        cy.wait(4000);
        cy.contains('Send via QR Code').click();
        cy.contains('Send OTP').click();
    });
});