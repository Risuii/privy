/// <reference types="cypress" />

describe('Sign and share test', () => {
    it('Sign and share', () => {
        cy.viewport(1280, 720);
        cy.visit('app.privy.id');
        cy.wait(3000);
        cy.get('[id=__BVID__4]').type('ini id');
        cy.contains('CONTINUE').click();
        cy.get('[id=__BVID__6]').type('ini pw');
        cy.contains('CONTINUE').click();
        cy.get('[id=v-step-0]').click();
        cy.contains('Sign & Request').click();
        cy.wait(2000);
        cy.get('input[type="file"]').attachFile('test.pdf',{ subjectType: 'drag-n-drop' });
        cy.xpath(`/html/body/div[4]/div[1]/div/div/footer`).click();
        cy.get('button[id="step-document-1"]',{timeout:20000}).click();
        cy.contains('Done').click();
        cy.wait(4000);
        cy.contains('Send via QR Code').click();
        cy.contains('Send OTP').click();
        cy.wait(20000);
        cy.get('[id=v-recipient-1]').click();
        cy.get('div.multiselect__tags').type('XC4809');
        cy.wait(7000);
        cy.get('ul.multiselect__content').click();
        cy.wait(10000);
        cy.xpath(`/html/body/div[1]/div/div/div/div[4]/div/div/div[3]/div/div/div/div[1]/div/span/div/div[1]/div/span/fieldset[2]/div/div/div`,{timeout:10000}).first().click();
        cy.xpath(`/html/body/div[1]/div/div/div/div[4]/div/div/div[3]/div/div/div/div[1]/div/span/div/div[1]/div/span/fieldset[3]/div/div/div[2]`,{timeout:2000}).click();
        cy.wait(5000);
        cy.xpath(`/html/body/div[1]/div/div/div/div[4]/div/div/div[3]/div/div/div/div[1]/div/span/div/div[1]/div/span/fieldset[3]/div/div/div[3]/ul/li[1]/span`,{timeout:10000}).click();
        cy.xpath(`/html/body/div[1]/div/div/div/div[4]/div/div/div[3]/div/div/div/div[1]/div/span/div/div[1]/div/span/div[2]/div[2]/button`).click();
        cy.xpath(`/html/body/div[1]/div/div/div/div[4]/div/div/div[3]/div/div/div/div[2]/div/div/div[2]/fieldset/div/div/div/div[2]/button`,{timeout:10000}).click();
    });
});