/// <reference types="cypress" />

describe('Change image signature test', () => {
    it('Change Image Signature', () => {
     cy.visit('app.privy.id')
     cy.wait(3000);
     cy.get('[id=__BVID__4]').type('ini id');
     cy.contains('CONTINUE').click();
     cy.get('[id=__BVID__6]').type('ini pw');
     cy.contains('CONTINUE').click();
     cy.contains('Change My Signature Image').click();
     cy.wait(8000)
     cy.contains('Add Signature').click();
     cy.wait(2000)
     cy.contains('Image').click();
     cy.xpath(`/html/body/div[3]/div[1]/div/div/div/div/div/div/div[1]/fieldset[1]/div/div/div`, {timeout:7000}).click();
     cy.contains('Click to Upload Image').click();
     cy.get('#upload-image___BV_modal_body_ > div:nth-child(3) > input[type="file"]').attachFile('ttd.png');
     cy.wait(2000)
     Cypress.on('uncaught:exception', (err, runnable) => {
         return false
     })
     cy.get('#upload-image___BV_modal_body_ > div:nth-child(4) > button.btn.btn-danger').click();
     cy.contains('Apply').click();
     cy.xpath(`/html/body/div[3]/div[1]/div/div/div/div/div/div/div[1]/fieldset[2]/div/div/div`, {timeout:7000}).click();
     cy.get('#upload-image___BV_modal_body_ > div:nth-child(3) > input[type=file]').attachFile('ttd.png');
     cy.wait(2000)
     cy.get('#upload-image___BV_modal_body_ > div:nth-child(4) > button.btn.btn-danger').click();
     cy.contains('Apply').click();
     cy.contains('Save').click();
    })
 });
 